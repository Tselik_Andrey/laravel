@extends('admin.layouts.layout')


@section('headerBlock')
    <div class="container"><br><br><br><br>
        <div class="col-sm-3 col-md-4  ">
            <h3 >Управление заказами</h3>
        </div>
        <form class="navbar-form navbar-right">
            <p><a class="btn btn-success" href="/admin/orders/create" role="button">Создать_Заказ</a></p>
        </form>
    </div>
    <hr>
@endsection


@section('content')
    <div class="col-sm-3 col-md-2 ">
        <ul class="nav nav-sidebar">
            <li><a class="navbar-brand" href="/admin">Главнвя_Адм.</a></li>
            <li><a class="navbar-brand" href="/admin/posts">Посты</a></li>
            <li><a class="navbar-brand" href="/admin/products">Продукты</a></li>
            <li><a class="navbar-brand" href="/admin/orders">Заказы</a></li>
            <li><a class="navbar-brand" href="/admin/pages">Страници</a></li>
            <li><a class="navbar-brand" href="/admin/admins">Администраторы</a></li>
            <li><a class="navbar-brand" href="/admin/clients">Клиенты</a></li>
        </ul>
    </div>

    <div class="col-sm-9  col-md-10 " >
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>НАЗВАНИЕ</th>
                    <th>ТЕЛЕФОН</th>
                    <th>ПРОСМОТР</th>
                    <th>ИЗМЕНИТЬ</th>
                    <th>УДАЛИТЬ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders AS $order)
                    <tr>
                        <td>
                            <h4>{{$order->customer_name}}</h4>
                        </td>
                        <td>
                            <span style="color: #5cb85c;"> Телефон:{{$order->phone}}</span><br>
                        </td>

                        <td>
                            <p><a class="btn btn-default" href="/admin/orders/{{$order->id}}" role="button">Подробнее »</a></p>
                        </td>
                        <td>
                            <p><a class="btn btn-warning" href="/admin/orders/{{$order->id}}/edit" role="button">Редактировать »</a></p>
                        </td>
                        <td>
                            <form action="/admin/orders/{{$order->id}}" method="POST">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" value="Удалить »" class="btn btn-danger">
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection


@section('footerBlock')
    <div class="container">
        <footer>
            <div class="col-md-4"><b>Hillel</b></div>
            <div class="col-md-4"><b>© 2017 Company, Inc.</b></div>
            <div class="col-md-4"><b>Tselik_Andrey</b></div>

        </footer>
    </div>
@endsection