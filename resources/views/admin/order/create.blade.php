@extends('layouts.layout')

@section('headerBlock')
    <div class="container">
        <h1>Create new order !!!:</h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-push-2">
            <form method="post" action="/admin/orders">

                <input type="hidden" name="_method" value="PUT">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="customer_name">Customer_name:</label>
                    <input name="customer_name" type="text" id="customer_name" class="form-control">
                </div>

                <div class="form-group">
                    <label for="email">Email:</label>
                    <input name="email" type="text" id="email" class="form-control">
                </div>

                <div class="form-group">
                    <label for="phone">Phone:</label>
                    <input name="phone" type="text" id="phone" class="form-control">
                </div>

                <div class="form-group">
                    <label for="feedback">Feedback:</label>
                    <textarea name="feedback" id="feedback" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary">Submit</button>
                </div>
            </form>

            @include('layouts.formError')
        </div>
    </div>
@endsection