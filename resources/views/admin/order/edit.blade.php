@extends('layouts.layout')

@section('headerBlock')
    <div class="container">
        <h1>Изменить заказ:</h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-push-2">
            <form method="post" action="/admin/orders/{{$order->id}}">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="customer_name">Имя клиента:</label>
                    <input name="customer_name" value="{{$order->customer_name}}" type="text" id="customer_name" class="form-control">
                </div>

                <div class="form-group">
                    <label for="email">Емейл клиента:</label>
                    <input name="email" value="{{$order->email}}" type="text" id="email" class="form-control">
                </div>

                <div class="form-group">
                    <label for="phone">Телефон клиента:</label>
                    <input name="phone" value="{{$order->phone}}" type="text" id="phone" class="form-control">
                </div>

                <div class="form-group">
                    <label for="feedback">Отзыв клиента:</label>
                    <textarea name="feedback" id="feedback" class="form-control">{{$order->feedback}}</textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary">Submit</button>
                </div>
            </form>

            @include('layouts.formError')

        </div>
    </div>
@endsection