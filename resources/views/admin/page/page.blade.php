@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2>{{$page->title}}</h2>
            <br>
            <p>{{$page->content}}</p>

        </div>
    </div>
@endsection