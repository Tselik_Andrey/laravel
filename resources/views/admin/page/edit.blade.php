@extends('layouts.layout')

@section('headerBlock')
    <div class="container">
        <h1>Изменить страницу:</h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-push-2">
            <form method="post" action="/admin/pages/{{$page->alias}}">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="title">Название страници:</label>
                    <input name="title" value="{{$page->title}}" type="text" id="title" class="form-control">
                </div>

                <div class="form-group">
                    <label for="alias">Ключевое слово:</label>
                    <input name="alias" value="{{$page->alias}}" type="text" id="alias" class="form-control">
                </div>

                <div class="form-group">
                    <label for="intro">Вступление:</label>
                    <input name="intro" value="{{$page->intro}}" type="text" id="intro" class="form-control">
                </div>

                <div class="form-group">
                    <label for="content">Текст страници:</label>
                    <textarea name="content" id="content" class="form-control">{{$page->content}}</textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary">Submit</button>
                </div>

            </form>

            @include('layouts.formError')

        </div>
    </div>
@endsection