@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2>{{$product->title}}</h2>
            <p>
                {{$product->price}}
            </p>
            <p>
                {{$product->description}}
            </p>
        </div>
    </div>
@endsection