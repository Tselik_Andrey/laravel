@extends('layouts.layout')

@section('headerBlock')
    <div class="container">
        <h1>Create new Product</h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-push-2">
            <form method="post" action="/admin/products">

                <input type="hidden" name="_method" value="PUT">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="title">Название:</label>
                    <input name="title" type="text" id="title" class="form-control">
                </div>

                <div class="form-group">
                    <label for="alias">Ключевые слова:</label>
                    <input name="alias" type="text" id="alias" class="form-control">
                </div>

                <div class="form-group">
                    <label for="price">Цена:</label>
                    <input name="price" type="text" id="price" class="form-control">
                </div>

                <div class="form-group">
                    <label for="description">Описание:</label>
                    <textarea name="description" id="description" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary">Submit</button>
                </div>
            </form>
            @include('layouts.formError')
        </div>
    </div>
@endsection