@extends('admin.layouts.layout')

@section('headerBlock')
    <div class="container">
        <h1>Post edit:</h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-push-2">
            <form method="post" action="/admin/products/{{$product->alias}}">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="title">Название товара:</label>
                    <input name="title" value="{{$product->title}}" type="text" id="title" class="form-control">
                </div>

                <div class="form-group">
                    <label for="alias">Ключевое слово:</label>
                    <input name="alias" value="{{$product->alias}}" type="text" id="alias" class="form-control">
                </div>

                <div class="form-group">
                    <label for="price">Вступление:</label>
                    <input name="price" value="{{$product->price}}" type="text" id="price" class="form-control">
                </div>

                <div class="form-group">
                    <label for="description">Описание товара:</label>
                    <textarea name="description" id="description" class="form-control">{{$product->description}}</textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary">Submit</button>
                </div>

            </form>

            @include('layouts.formError')

        </div>
    </div>
@endsection