@extends('admin.layouts.layout')


@section('headerBlock')
    <div class="container"><br><br><br><br>
        <div class="col-sm-3 col-md-4  ">
            <h3 >ВСЕ ТАБЛИЦИ</h3>
        </div>
        <div class="col-sm-3 col-md-2  ">
            <form class="navbar-form navbar-right">
        <p><a class="btn btn-success" href="/admin/posts/create" role="button">Создать_Пост</a></p>
            </form>
        </div>
        <div class="col-sm-3 col-md-2  ">
            <form class="navbar-form navbar-right">
        <p><a class="btn btn-success" href="/admin/pages/create" role="button">Создать_Страницу</a></p>
            </form>
        </div>
        <div class="col-sm-3 col-md-2  ">
            <form class="navbar-form navbar-right">
        <p><a class="btn btn-success" href="/admin/orders/create" role="button">Создать_Заказ</a></p>
            </form>
        </div>
        <div class="col-sm-3 col-md-2  ">
            <form class="navbar-form navbar-right">
        <p><a class="btn btn-success" href="/admin/products/create" role="button">Создать_Продукт</a></p>
            </form>
        </div>

    </div>
    <hr>
@endsection


@section('content')

            <div class="col-sm-3 col-md-2 ">
                <ul class="nav nav-sidebar">
                    <li><a class="navbar-brand" href="/admin">Главнвя_Адм.</a></li>
                    <li><a class="navbar-brand" href="/admin/posts">Посты</a></li>
                    <li><a class="navbar-brand" href="/admin/products">Продукты</a></li>
                    <li><a class="navbar-brand" href="/admin/orders">Заказы</a></li>
                    <li><a class="navbar-brand" href="/admin/pages">Страници</a></li>
                    <li><a class="navbar-brand" href="/admin/admins">Администраторы</a></li>
                    <li><a class="navbar-brand" href="/admin/clients">Клиенты</a></li>
                </ul>
            </div>

            <div class="col-sm-9  col-md-10 " >

                <div class="table-responsive">
                    <H3 style="color: #5cb85c;"> ВСЕ ПОСТЫ</H3>
                    <table class="table table-striped">
                        <thead style="background-color: #f0ad4e;">
                        <tr>
                            <th>НАЗВАНИЕ</th>
                            <th>КЛЮЧ.СЛОВО</th>
                            <th>ПРОСМОТР</th>
                            <th>ИЗМЕНИТЬ</th>
                            <th>УДАЛИТЬ</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts AS $post)
                        <tr>
                            <td>
                                <h4>{{$post->title}}</h4>
                            </td>
                            <td>
                                <span style="color: #5cb85c;"> Телефон:{{$post->alias}}</span><br>
                            </td>

                            <td>
                                <p><a class="btn btn-default" href="/posts/{{$post->alias}}" role="button">Подробнее »</a></p>
                            </td>
                            <td>
                                <p><a class="btn btn-warning" href="/admin/posts/{{$post->alias}}/edit" role="button">Редактировать »</a></p>
                            </td>
                            <td>
                                <form action="/admin/posts/{{$post->alias}}" method="POST">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" value="Удалить »" class="btn btn-danger">
                                </form>
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <br><br>
                <div class="table-responsive">
                    <H3 style="color: #5cb85c;"> ВСЕ ЗАКАЗЫ</H3>
                    <table class="table table-striped">
                        <thead style="background-color: #f0ad4e;">
                            <tr>
                                <th>НАЗВАНИЕ</th>
                                <th>ТЕЛЕФОН</th>
                                <th>ПРОСМОТР</th>
                                <th>ИЗМЕНИТЬ</th>
                                <th>УДАЛИТЬ</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($orders AS $order)
                            <tr>
                                <td>
                                    <h4>{{$order->customer_name}}</h4>
                                </td>
                                <td>
                                    <span style="color: #5cb85c;"> Телефон:{{$order->phone}}</span><br>
                                </td>

                                <td>
                                    <p><a class="btn btn-default" href="/admin/orders/{{$order->id}}" role="button">Подробнее »</a></p>
                                </td>
                                <td>
                                    <p><a class="btn btn-warning" href="/admin/orders/{{$order->id}}/edit" role="button">Редактировать »</a></p>
                                </td>
                                <td>
                                    <form action="/admin/orders/{{$order->id}}" method="POST">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" value="Удалить »" class="btn btn-danger">
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

                <br><br>

                <div class="table-responsive">
                    <H3 style="color: #5cb85c;" > ВСЕ СТРАНИЦИ</H3>
                    <table class="table table-striped">
                        <thead style="background-color: #f0ad4e;">
                        <tr>
                            <th>НАЗВАНИЕ</th>
                            <th>ВСТУПЛЕНИЕ</th>
                            <th>ПРОСМОТР</th>
                            <th>ИЗМЕНИТЬ</th>
                            <th>УДАЛИТЬ</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pages AS $page)
                            <tr>
                                <td>
                                    <h4>{{$page->title}}</h4>
                                </td>
                                <td>
                                    <b>{{$page->intro}}</b>
                                </td>

                                <td>
                                    <p><a class="btn btn-default" href="/pages/{{$page->alias}}" role="button">Подробнее »</a></p>
                                </td>
                                <td>
                                    <p><a class="btn btn-warning" href="/admin/pages/{{$page->alias}}/edit" role="button">Редактировать »</a></p>
                                </td>
                                <td>
                                    <form action="/admin/pages/{{$page->alias}}" method="POST">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" value="Удалить »" class="btn btn-danger">
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

                <br><br>

                <div class="table-responsive">
                    <H3 style="color: #5cb85c;" > ВСЕ ПРОДУКТЫ</H3>
                    <table class="table table-striped">
                        <thead style="background-color: #f0ad4e;">
                        <tr>
                            <th>НАЗВАНИЕ</th>
                            <th>ТЕЛЕФОН</th>
                            <th>ПРОСМОТР</th>
                            <th>ИЗМЕНИТЬ</th>
                            <th>УДАЛИТЬ</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products AS $product)
                            <tr>
                                <td>
                                    <h4>{{$product->title}}</h4>
                                </td>
                                <td>
                                    <span >{{$product->price}} USD</span>
                                </td>

                                <td>
                                    <p><a class="btn btn-default" href="/products/{{$product->alias}}" role="button">Подробнее »</a></p>
                                </td>
                                <td>
                                    <p><a class="btn btn-warning" href="/admin/products/{{$product->alias}}/edit" role="button">Редактировать »</a></p>
                                </td>
                                <td>
                                    <form action="/admin/products/{{$product->alias}}" method="POST">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" value="Удалить »" class="btn btn-danger">
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>



            </div>
@endsection


@section('footerBlock')
    <div class="container" style="display: inherit">
        <footer>
            <div class="col-md-4"><b>Hillel</b></div>
            <div class="col-md-4"><b>© 2017 Company, Inc.</b></div>
            <div class="col-md-4"><b>Tselik_Andrey</b></div>

        </footer>
    </div>
@endsection
<!--                --><?php //dd($pages); ?><!--   -->