@extends('layouts.layout')

@section('headerBlock')
    <div class="container">
        <h1>Post edit:</h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-push-2">
            <form method="post" action="/admin/posts/{{$post->alias}}">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="title">Title:</label>
                    <input name="title" value="{{$post->title}}" type="text" id="title" class="form-control">
                </div>

                <div class="form-group">
                    <label for="alias">Alias:</label>
                    <input name="alias" value="{{$post->alias}}" type="text" id="alias" class="form-control">
                </div>

                <div class="form-group">
                    <label for="intro">Intro:</label>
                    <input name="intro" value="{{$post->intro}}" type="text" id="intro" class="form-control">
                </div>

                <div class="form-group">
                    <label for="body">Body:</label>
                    <textarea name="body" id="body" class="form-control">{{$post->body}}</textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary">Submit</button>
                </div>

            </form>

            @include('layouts.formError')

        </div>
    </div>
@endsection