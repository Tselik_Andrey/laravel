@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2>{{$post->title}}</h2>
            <p>
                {{$post->intro}}
            </p>
            <p>
                {{$post->body}}
            </p>
        </div>
    </div>
@endsection