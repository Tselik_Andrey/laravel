<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body>


    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" style="color: #5cb85c" href="/admin">ADMIN_PANEL</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">

            @if(Auth::check())
                <form class="navbar-form navbar-right">
                    <a class="btn btn-success href="#">Hello {{ Auth::user()->name }}</a>
                </form>
            @else
                <form class="navbar-form navbar-right">
                    <a class="btn btn-success" href="/users/create">REGISTRATION</a>
                </form>
                <form class="navbar-form navbar-right">
                    <a  class="btn btn-success" href="#">sign in</a>
                </form>
            @endif

            <form class="navbar-form navbar-right">
                <p><a class="btn btn-success" href="/" role="button">ВЕРНУТЬСЯ НА ГЛАВНУЮ</a></p>
            </form>
        </div><!--/.navbar-collapse -->
        </div>
    </div>

    <div class="container">

    @yield('headerBlock')

    @yield('content')

    @yield('footerBlock')

    </div>


</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</html>