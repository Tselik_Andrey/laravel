@extends('layouts.layout')

@section('content')
    <div class="row">
        @foreach($orders AS $order)
        <div class="col-md-12">
            <h4>{{$order->customer_name}}</h4>
            <span style="color: #5cb85c;"> Телефон:{{$order->phone}}</span><br>
            <span style="color: #5cb85c;"> Email:{{$order->email}}</span>
            <p><a class="btn btn-default" href="/orders/{{$order->id}}" role="button">Подробнее »</a></p>
        </div>
        @endforeach
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>All orders!</h1>
    </div>
@endsection

@section('footerBlock')
    <div class="container">
        <footer>
            <div class="col-md-4"><b>Hillel</b></div>
            <div class="col-md-4"><b>© 2017 Company, Inc.</b></div>
            <div class="col-md-4"><b>Tselik_Andrey</b></div>

        </footer>
    </div>
@endsection