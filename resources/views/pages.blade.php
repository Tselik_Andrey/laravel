@extends('layouts.layout')


@section('content')
    <div class="row">
        @foreach($pages AS $page)
        <div class="col-md-12">
            <h4>{{$page->title}}</h4>
            <span style="color: #5cb85c;"> {{$page->intro}}</span><br>

            <p><a class="btn btn-default" href="/pages/{{$page->alias}}" role="button">Читать далее »</a></p>
        </div>
        @endforeach
    </div>
@endsection


@section('headerBlock')
    <div class="container">
        <h1>All Pages!</h1>
    </div>
@endsection

@section('footerBlock')
    <div class="container">
        <footer>
            <div class="col-md-4"><b>Hillel</b></div>
            <div class="col-md-4"><b>© 2017 Company, Inc.</b></div>
            <div class="col-md-4"><b>Tselik_Andrey</b></div>

        </footer>
    </div>
@endsection