<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" style="color: #5cb85c" href="/">HILLEL_SHOP</a>
            <a class="navbar-brand" href="/products">Products</a>
            <a class="navbar-brand" href="/orders">Orders</a>
            <a class="navbar-brand" href="/pages">Pages</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            @if(Auth::check())
                <form class="navbar-form navbar-right">
                <a class="btn btn-success" href="#">Hello {{ Auth::user()->name }}</a>
                </form>
            @else
                <form class="navbar-form navbar-right">
                    <a class="btn btn-success" href="/users/create">REGISTRATION</a>
                </form>
                <form class="navbar-form navbar-right">
                    <a  class="btn btn-success" href="#">sign in</a>
                </form>
            @endif

            <form class="navbar-form navbar-right">
                <p><a class="btn btn-success" href="/admin" role="button">Панель Ад_министр_атора »</a></p>
            </form>

        </div><!--/.navbar-collapse -->
    </div>
</nav>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">

    @yield('headerBlock')

</div>

<div class="container">

    @yield('content')

    <hr><hr><hr><hr><hr><hr>

    @yield('footerBlock')


</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</html>