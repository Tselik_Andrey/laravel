@extends('layouts.layout')

@section('content')
    <div class="row">
        @foreach($products AS $product)
        <div class="col-md-12">
            <h3>{{$product->title}}</h3>
            <p style="color: #5cb85c;">{{$product->price}} usd</p>
            <p><a class="btn btn-default" href="/products/{{$product->alias}}" role="button">Подробнее »</a></p>
        </div>
        @endforeach
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>All Products!</h1>
    </div>
@endsection


@section('footerBlock')
    <div class="container">
        <footer>
            <div class="col-md-4"><b>Hillel</b></div>
            <div class="col-md-4"><b>© 2017 Company, Inc.</b></div>
            <div class="col-md-4"><b>Tselik_Andrey</b></div>

        </footer>
    </div>
@endsection


