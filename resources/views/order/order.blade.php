@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <h2 style="color: #3869D4">{{$order->customer_name}}</h2>
            <br>
            <b style="color: #2ca02c"> Телефон:{{$order->phone}}</b>
            <br>
            <b style="color: #2ca02c">Email:{{$order->email}}</b>
            <br>
            <p>{{$order->feedback}}</p>
        </div>
    </div>
@endsection