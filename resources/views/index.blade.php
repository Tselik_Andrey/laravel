@extends('layouts.layout')

@section('content')
    <div class="row">
        @foreach($posts AS $post)
        <div class="col-md-12">
            <h2>{{$post->title}}</h2>
            <p>{{$post->intro}}</p>
            <p><a class="btn btn-default" href="/posts/{{$post->alias}}" role="button">Читать далее »</a></p>
        </div>
        @endforeach
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1> All Posts!</h1>
    </div>
@endsection

@section('footerBlock')
    <div class="container">
        <footer>
            <div class="col-md-4"><b>Hillel</b></div>
            <div class="col-md-4"><b>© 2017 Company, Inc.</b></div>
            <div class="col-md-4"><b>Tselik_Andrey</b></div>

        </footer>
    </div>
@endsection