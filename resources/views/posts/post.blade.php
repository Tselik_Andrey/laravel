@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2>{{$post->title}}</h2>
            <p>
                {{$post->intro}}
            </p>
            <p>
                {{$post->body}}
            </p>
            <div class="comments">
                <ul class="list-group">
                    @foreach($post->comments as $comment)
                        <li class="list-group-item">
                            {{$comment->created_at->diffForHumans()}}
                            |
                            <b>{{$comment->body}}</b>
                        </li>
                    @endforeach
                </ul>
            </div>

            <div class="card">
                <div class="card-block">
                    <form method="POST" action="/posts/{{$post->alias}}/comments">

                        {{csrf_field()}}

                        {{method_field('PUT')}}

                        <div class="form-group">
                            <label for="comment_body">Enter your comment:</label>
                            <textarea name="body" id="comment_body" class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Post it!</button>
                        </div>

                    </form>
                </div>
            </div>


        </div>
    </div>
@endsection