<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
                array(
                    'customer_name' => 'Valentin Derizemlya',
                    'phone' => '+38(063)6549820',
                    'email' => 'Valet@gmail.com',
                    'feedback' => 'Запер соседку, стирал два часа, теперь ремонт по ночам не делает! Всем кул, и Хорошего настроения'
                ),
                array(
                    'customer_name' => 'Ольга Орлова',
                    'phone' => '+38(063)5789820',
                    'email' => 'Ola@gmail.com',
                    'feedback' => 'Начинаю стирать, в жк Кадор гаснет свет на 12 часов, Очень нравится , то что нужно'
                ),
                array(
                    'customer_name' => 'Хабиб',
                    'phone' => '+38(063)7777777',
                    'email' => 'hab@gmail.com',
                    'feedback' => 'Нашяльнике каак укккулючить, не панимать што тама написана ма !!'
                )]
        );
    }
}
