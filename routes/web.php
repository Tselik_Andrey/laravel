<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// MAIN
Route::get('/', "PostsController@index");

Route::get('/posts/{post}', "PostsController@show");

Route::put('/posts/{post}/comments', "CommentsController@store");


//PRODUCTS
Route::get('/products', "ProductsController@index");

Route::get('/products/{product}', "ProductsController@show");

Route::put('/products/{product}/comments', "CommentsController@storeProduct");


//ORDERS
Route::get('/orders', "OrdersController@index");

Route::get('/orders/{order}', "OrdersController@show");


//PAGES
Route::get('/pages', "PagesController@index");

Route::get('/pages/{page}', "PagesController@show");

Route::put('/pages/{page}/comments', "CommentsController@storePage");




// ---------------------------------------------------------ADMIN-MAIN

//AUTH USER
Route::get('/users/create', "RegistrationController@create");

Route::put('/users', "RegistrationController@store");



Route::get('/admin', "Admin\\AdminController@index");

Route::put('/admin/posts', "Admin\\PostsController@store");

Route::get('/admin/posts/{post}/edit', "Admin\\AdminController@edit");

Route::post('/admin/posts/{post}', "Admin\\AdminController@update");

Route::delete('/admin/posts/{post}', "Admin\\AdminController@destroy");


// POSTS
Route::get('/admin/posts', "Admin\\PostsController@index");

Route::get('/admin/posts/create', "Admin\\PostsController@create");

Route::put('/admin/posts', "Admin\\PostsController@store");

Route::get('/admin/posts/{post}/edit', "Admin\\PostsController@edit");

Route::post('/admin/posts/{post}', "Admin\\PostsController@update");

Route::delete('/admin/posts/{post}', "Admin\\PostsController@destroy");


// PAGES
Route::get('/admin/pages', "Admin\\PagesController@index");

Route::get('/admin/pages/create', "Admin\\PagesController@create");

Route::put('/admin/pages', "Admin\\PagesController@store");

Route::get('/admin/pages/{page}/edit', "Admin\\PagesController@edit");

Route::post('/admin/pages/{page}', "Admin\\PagesController@update");

Route::delete('/admin/pages/{page}', "Admin\\PagesController@destroy");


// PRODUCTS
Route::get('/admin/products', "Admin\\ProductsController@index");

Route::get('/admin/products/create', "Admin\\ProductsController@create");

Route::put('/admin/products', "Admin\\ProductsController@store");

Route::get('/admin/products/{product}/edit', "Admin\\ProductsController@edit");

Route::post('/admin/products/{product}', "Admin\\ProductsController@update");

Route::delete('/admin/products/{product}', "Admin\\ProductsController@destroy");


// ORDERS
Route::get('/admin/orders', "Admin\\OrdersController@index");

Route::get('/admin/orders/create', "Admin\\OrdersController@create");

Route::get('/admin/orders/{order}', "Admin\\OrdersController@show");

Route::put('/admin/orders', "Admin\\OrdersController@store");

Route::get('/admin/orders/{order}/edit', "Admin\\OrdersController@edit");

Route::post('/admin/orders/{order}', "Admin\\OrdersController@update");

Route::delete('/admin/orders/{order}', "Admin\\OrdersController@destroy");


