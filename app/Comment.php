<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable = ['post_id', 'page_id', 'product_id', 'body'];

    public function post(){

        return $this->belongsTo(Post::class);
    }

    public function page(){

        return $this->belongsTo(Page::class);

    }

    public function product(){

        return $this->belongsTo(Product::class);

    }

    public function user(){

        return $this->belongsTo(User::class);

    }
}
