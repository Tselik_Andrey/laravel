<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 21.06.2017
 * Time: 14:01
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = ['title', 'alias', 'price', 'description'];
    //  protected $guarded = ['title'];

    public function getRouteKeyName()
    {
     // return 'id';
        return 'alias';
    }

    public function comments(){

        return $this->hasMany(Comment::class);
    }

    public function user(){

        return $this->belongsTo(User::class);

    }

}
