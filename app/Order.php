<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 21.06.2017
 * Time: 14:01
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = ['customer_name', 'email', 'phone', 'feedback'];
    //  protected $guarded = ['title'];

    public function getRouteKeyName()
    {
       return 'id';
       // return 'alias';
    }

}
