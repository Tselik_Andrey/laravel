<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{

    public function index()
    {
        $data['products'] = Product::all();
        return view('products', $data);
    }

    public function show(Product $product)
    {
        $data['product'] = $product;
        return view('product.product')->with($data);

    }


}