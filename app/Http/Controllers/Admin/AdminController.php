<?php
namespace App\Http\Controllers\Admin;

use App\Post;
use App\Page;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller{


    public function index()
    {
    $data['pages'] = Page::all();
    $data['orders'] = Order::all();
    $data['products'] = Product::all();
    $data['posts'] = Post::all();

    return view('admin.index', $data);
    }



    public function show(Post $post)
    {
        $data['post'] = $post;
        return view('posts.post')->with($data);
    }


    public function create()
    {
        return view('admin.posts.create');
    }


    public function store()
    {
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'intro' => 'required',
            'body' => 'required',
        ]);

        Post::create(request(['title', 'alias', 'intro', 'body']));
        return redirect('/admin');
    }

    public function edit(Post $post){
        return view('admin.posts.edit', compact('post'));
    }

    // 4.2 Обработать форму редактирования
    public function update(Post $post){
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'intro' => 'required',
            'body' => 'required',
        ]);
        $post->update(request()->all());
        return redirect('/admin');
    }


    public function destroy(Post $post)
    {
        $post->delete();
        return redirect('/admin');
    }


}
