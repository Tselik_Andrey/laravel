<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function index()
    {
        $data['pages'] = Page::all();
        return view('admin.pages', $data);
    }


    public function show(Page $page){
        $data['page'] = $page;
        return view('page.page')->with($data);

    }


    public function create()
    {
        return view('admin.page.create');
    }


    public function store()
    {
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'intro' => 'required',
            'content' => 'required',
        ]);

        Page::create(request(['title', 'alias', 'intro', 'content']));
        return redirect('/admin/pages');
    }


    public function edit(Page $page){
        return view('admin.page.edit', compact('page'));
    }


    // 4.2 Обработать форму редактирования
    public function update(Page $page){
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'intro' => 'required',
            'content' => 'required',
        ]);
        $page->update(request()->all());
        return redirect('/admin/pages');
    }


    public function destroy(Page $page)
    {
        $page->delete();
        return redirect('/admin/pages');
    }
}
