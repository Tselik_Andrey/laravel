<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    public function index()
    {
        $data['orders'] = Order::all();
        return view('admin.orders', $data);
    }


    public function show(Order $order)
    {
        $data['order'] = $order;
        return view('admin.order.order')->with($data);
    }


    public function create()
    {
        return view('admin.order.create');
    }


    public function store()
    {
        $this->validate(request(), [
            'customer_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'feedback' => 'required',
        ]);

        Order::create(request(['customer_name', 'email', 'phone', 'feedback']));
        return redirect('/admin/orders');
    }


    public function edit(Order $order){
        return view('admin.order.edit', compact('order'));
    }


    // 4.2 Обработать форму редактирования
    public function update(Order $order){
        $this->validate(request(), [
            'customer_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'feedback' => 'required',
        ]);
        $order->update(request()->all());
        return redirect('/admin/orders');
    }


    public function destroy(Order $order)
    {
        $order->delete();
        return redirect('/admin/orders');
    }



}
