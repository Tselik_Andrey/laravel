<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public function index()
    {
        $data['products'] = Product::all();
        return view('admin.products', $data);
    }


    public function show(Product $product)
    {
        $data['product'] = $product;
        return view('product.product')->with($data);

    }


    public function create()
    {
        return view('admin.product.create');
    }


    public function store()
    {
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'price' => 'required',
            'description' => 'required',
        ]);

        Product::create(request(['title', 'alias', 'price', 'description']));
        return redirect('/admin/products');
    }


    public function edit(Product $product){
        return view('admin.product.edit', compact('product'));
    }


    public function update(Product $product){
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'price' => 'required',
            'description' => 'required',
        ]);
        $product->update(request()->all());
        return redirect('/admin/products');
    }


    public function destroy(Product $product)

    {
        $product->delete();
        return redirect('/admin/products');
    }


}
