<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegistrationController extends Controller
{
    public function create(){

        return view('auth.create');
    }


    public function store(){
        //Validate the form
        $this->validate(request(),[
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed'
        ]);

        //Create and save user
       $user = User::create(
            array_merge(
                request(['name','email']),
                ['password' => bcrypt(request('password'))]
            )
        );

        //Sign in user
        auth()->login($user);

        //Homepage
        return redirect('/');
    }


}
