<?php

namespace App\Http\Controllers;

use App\Post;
use App\Page;
use App\Product;
use App\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{

    public function store(Post $post)
    {
        Comment::create([
            'post_id' => $post->id,
            'body' => request('body')
        ]);
        return back();
    }


    public function storePage(Page $page)
    {
        Comment::create([
            'page_id' => $page->id,
            'body' => request('body')
        ]);
        return back();
    }

    public function storeProduct(Product $product)
    {
        Comment::create([
            'product_id' => $product->id,
            'body' => request('body')
        ]);
        return back();
    }

}
