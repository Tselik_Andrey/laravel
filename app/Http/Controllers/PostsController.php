<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;


class PostsController extends Controller
{

    public function index()
    {
        $data['posts'] = Post::all();
        return view('index', $data);
    }


    public function show(Post $post)
    {
        $data['post'] = $post;
        return view('posts.post')->with($data);
    }


}
