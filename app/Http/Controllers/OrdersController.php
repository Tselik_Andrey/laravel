<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{

    public function index()
    {
        $data['orders'] = Order::all();
        return view('orders', $data);
    }


    public function show(Order $order)
    {
        $data['order'] = $order;
        return view('order.order')->with($data);
    }



}