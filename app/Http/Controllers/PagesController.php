<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{

    public function index()
    {
        $data['pages'] = Page::all();
        return view('pages', $data);
    }


    public function show(Page $page){
        $data['page'] = $page;
        return view('page.page')->with($data);

    }


}
